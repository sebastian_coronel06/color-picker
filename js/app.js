const color = document.getElementById('color')
const salida = document.getElementById('salida')
const body = document.getElementById('body')


color.addEventListener('input', () => {
    //Esto cambia de color el body
    //const colorFondo = color.value;
    //body.style.background = colorFondo
    //salida.textContent = `${colorFondo}`

    const value = color.value
    salida.innerHTML = value
    salida.style.background = value
    salida.style.color = '#fff'
})